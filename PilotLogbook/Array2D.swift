//
//  Array2D.swift
//  PilotLogbook
//
//  Created by admin on 7/19/15.
//  Copyright (c) 2015 NWMSU. All rights reserved.
//

import Foundation

/****************************************
A two dimensional array for initializing flight details array in the AddFlightViewController
*****************************************/
class Array2D{
    var cols:Int
    var rows:Int
    var matrix:[NSString]
    
    init(cols:Int, rows:Int) {
        self.cols = cols
        self.rows = rows
        matrix = Array(count:cols*rows, repeatedValue: "")
    }
    
    subscript(row:Int, col:Int ) -> NSString {
        get {
            return matrix[cols * row + col]
        }
        set {
            matrix[cols*row+col] = newValue
        }
    }
}