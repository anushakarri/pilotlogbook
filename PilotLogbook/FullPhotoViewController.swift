//
//  FullPhotoViewController.swift
//  iGeeksMemories
//
//  Created by iGeeks on 4/21/15.
//  Copyright (c) 2015 Student. All rights reserved.
//


/****************************************
This class shows the full image of the flight when image is tapped on the LogbookDetailViewController
*****************************************/
import UIKit

class FullPhotoViewController: UIViewController,UITextViewDelegate, UIActionSheetDelegate ,UIImagePickerControllerDelegate , UINavigationControllerDelegate{
    
    @IBOutlet weak var dispImg: UIImageView!
    var image2 = UIImage()
    var sharedInfo:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    @IBOutlet weak var notes: UITextView!
    
    /****************************************
    Load the view with full image and for editing
    *****************************************/
    override func viewDidLoad() {
        super.viewDidLoad()
        if image2 == true  {
            dispImg.image = image2 }
        dispImg.image = UIImage(data: sharedInfo.logbookDetail.image)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Edit", style: UIBarButtonItemStyle.Plain, target: self, action: "editingImage:")
    }
    
    override func viewWillAppear(animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func textViewShouldEndEditing(textView: UITextView) -> Bool {
        textView.resignFirstResponder()
        return true
    }
    
    /****************************************
    Function for editing the image in the detail view controller
    *****************************************/
    func editingImage(sender:AnyObject){
        
        // iOS8:
        let alertController: UIAlertController = UIAlertController(title: "Choose the image", message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Back", style: UIAlertActionStyle.Cancel, handler: nil)
        let button1action: UIAlertAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.Default, handler: { (action: UIAlertAction) -> () in
            // doing something for "camera"
            let picker = UIImagePickerController()
            picker.delegate = self
            picker.sourceType = .Camera
            self.sharedInfo.isImageEdit = true
            self.presentViewController(picker, animated: true, completion :nil)
            
        })
        let button2action: UIAlertAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.Default, handler: { (action: UIAlertAction) -> () in
            // doing something for "gallery"
            let picker = UIImagePickerController()
            picker.sourceType = .PhotoLibrary
            picker.mediaTypes = UIImagePickerController.availableMediaTypesForSourceType(.PhotoLibrary)!
            picker.delegate = self
            picker.allowsEditing = true
            self.sharedInfo.isImageEdit = true
            self.presentViewController(picker, animated: true, completion: nil)
            
        })
        
        let button3action: UIAlertAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: { (action: UIAlertAction) -> () in
            
            self.sharedInfo.isImageEdit = false
            self.navigationController?.popViewControllerAnimated(true )
            
        })
        alertController.addAction(cancelAction)
        alertController.addAction(button1action)
        alertController.addAction(button2action)
        alertController.addAction(button3action)
        
        if let popoverController = alertController.popoverPresentationController {
            
            let view = UIView()
            
            popoverController.sourceView = view
            popoverController.sourceRect = view.bounds
            
        }
        self.presentViewController(alertController, animated: true, completion: nil)
        
    }
    
    /****************************************
    Function for picking an image from the gallery or camera
    *****************************************/
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        let image:UIImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        let screenSize:CGSize = UIScreen.mainScreen().bounds.size
        
        let newImage:UIImage = scalePhoto(image, size: CGSize(width: screenSize.width, height: screenSize.height))
        dispImg.image = newImage
        sharedInfo.editImage = dispImg.image!
        picker.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    /****************************************
    Function for cancelling the image picker
    *****************************************/
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        
        picker.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    /****************************************
    Function for scaling the image
    *****************************************/
    func scalePhoto(image:UIImage, size:CGSize) ->UIImage
    {
        let scale:CGFloat = max(size.width/image.size.width,size.height/image.size.height)
        let width:CGFloat = image.size.width * scale
        let height:CGFloat = image.size.height * scale
        
        let imageRect:CGRect = CGRectMake((size.width - width)/2.0, (size.height - height)/2.0, width, height)
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        
        image.drawInRect(imageRect)
        
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    @IBAction func cancel(sender: AnyObject) {
        self.dismissViewControllerAnimated(true , completion: nil )
    }
    
    
}
