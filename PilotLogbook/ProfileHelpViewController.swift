//
//  ProfileHelpViewController.swift
//  PilotLogbook
//
//  Created by admin on 6/25/15.
//  Copyright (c) 2015 NWMSU. All rights reserved.
//

/****************************************
This class is for the help of Profile
*****************************************/
import UIKit

class ProfileHelpViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Profile Help"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
