//
//  LogbookViewController.swift
//  PilotLogbook
//
//  Created by admin on 6/25/15.
//  Copyright (c) 2015 NWMSU. All rights reserved.
//

/****************************************
This class is used for displaying all the flights in the logbook
*****************************************/
import UIKit
import CoreData

class LogbookViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, UISearchBarDelegate {
    
    
    @IBOutlet weak var flightDetailsSeachBar: UISearchBar!
    
    var searchActive :Bool = false
    var sortDescriptor : NSSortDescriptor!
    @IBOutlet var flightDetailsTV: UITableView!
    
    var appy = UIApplication.sharedApplication().delegate as! AppDelegate
    var managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    var details:[FlightDetails]!
    
    enum tagValues : Int {
        case fromTag = 100, toTag = 101, identTag = 102, dateTag = 103, actInstrTag = 104, picTag = 105, totalTimeTag = 106, flightImgTag = 99
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        flightDetailsSeachBar.delegate = self
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "refreshData", name: "updatedRecord", object: nil)
       
        // Do any additional setup after loading the view.
    }
    
    /****************************************
    Fetching the flight details from the database and reloading the table
    *****************************************/
    override func viewWillAppear(animated: Bool){
        super.viewWillAppear(animated)
//        let fetchRequest = NSFetchRequest(entityName:"FlightDetails")
//        details = (try! managedObjectContext?.executeFetchRequest(fetchRequest)) as! [FlightDetails]
//        details = Array(details.reverse())
//        flightDetailsTV.reloadData()
        refreshData()
    }
    
    func refreshData(){
        Swift.print(" ")
        Swift.print("refreshData")
        let fetchRequest = NSFetchRequest(entityName:"FlightDetails")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "date", ascending : false)]
        details = (try! managedObjectContext?.executeFetchRequest(fetchRequest)) as! [FlightDetails]
       
        flightDetailsTV.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /****************************************
    Loading the labels in the view controller with the values "from","to","ident","date","actual instruments","simulated instruments","PIC","total time" from the database
    *****************************************/
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("flightCell", forIndexPath: indexPath)
        
        let from = cell.viewWithTag(tagValues.fromTag.rawValue) as! UILabel
        let to = cell.viewWithTag(tagValues.toTag.rawValue) as! UILabel
        let ident = cell.viewWithTag(tagValues.identTag.rawValue) as! UILabel
        let date = cell.viewWithTag(tagValues.dateTag.rawValue) as! UILabel
        let actual_instruments = cell.viewWithTag(tagValues.actInstrTag.rawValue) as! UILabel
        let pilot_in_command = cell.viewWithTag(tagValues.picTag.rawValue) as! UILabel
        let total_time = cell.viewWithTag(tagValues.totalTimeTag.rawValue) as! UILabel
        from.text = details[indexPath.row].from
        to.text = details[indexPath.row].to
        ident.text = details[indexPath.row].ident
        let dateFormatter = NSDateFormatter() // formatting the date into required format
        dateFormatter.dateFormat = "dd MMM yyyy"
        date.text = dateFormatter.stringFromDate(details[indexPath.row].date)
        actual_instruments.text = details[indexPath.row].actual_Instruments.description
        pilot_in_command.text = details[indexPath.row].pilot_In_Command.description
        total_time.text = details[indexPath.row].total_Time.description
        //cam-start
        let flightImg = cell.viewWithTag(tagValues.flightImgTag.rawValue) as! UIImageView
        flightImg.image = UIImage(data: details[indexPath.row].image)
        //cam-end
        
        return cell
    }
    
    /****************************************
    Function for number of sections in the table view
    *****************************************/
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    /****************************************
    Function for number of rows in the tableview
    *****************************************/
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return details.count
    }
    
    /****************************************
    Function called when a cell is tapped in the table view. A detail view controller appears on selecting a row
    *****************************************/
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let logbookDetails:LogbookDetailViewController = self.storyboard?.instantiateViewControllerWithIdentifier("logbookDetails") as! LogbookDetailViewController
        logbookDetails.image1 = checkNillImage(UIImage(data: details[indexPath.row].image)!)
        if(UIImage(data: details[indexPath.row].image) != nil)
        {
            logbookDetails.image1 = UIImage(data: details[indexPath.row].image)
        }
        else
        {
            logbookDetails.image1 = UIImage(named:"AddImage.jpg")!
        }
        appy.logbookDetail = details[indexPath.row] as FlightDetails
        self.navigationController?.pushViewController(logbookDetails, animated: true)
    }
    
    /****************************************
    Funciton for deleting a flight record from the table
    *****************************************/
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == UITableViewCellEditingStyle.Delete {
            
            let cell: UITableViewCell = tableView.cellForRowAtIndexPath(indexPath) as UITableViewCell!
            let deleteAlert = UIAlertController(title :"Delete Flight" ,message:"Do you want to delete the flight record?", preferredStyle : UIAlertControllerStyle.ActionSheet )
            deleteAlert.addAction(UIAlertAction(title:"Delete",style : UIAlertActionStyle.Default, handler : {action in self.deleteRecord(indexPath)}))
            deleteAlert.addAction(UIAlertAction(title:"Cancel",style : UIAlertActionStyle.Default, handler : {action in deleteAlert.dismissViewControllerAnimated(true, completion: nil)
                self.flightDetailsTV.reloadData()
            }))
            
            let popOver = deleteAlert.popoverPresentationController
            popOver?.sourceView  = cell as UIView
            popOver?.sourceRect = (cell as UIView).bounds
            popOver?.permittedArrowDirections = UIPopoverArrowDirection.Any
            popOver?.permittedArrowDirections = UIPopoverArrowDirection.Any
            self.presentViewController(deleteAlert, animated: true, completion:nil)
        }
    }
    
    /****************************************
    Function to delete a flight record from the database
    *****************************************/
    func deleteRecord(indexPath :  NSIndexPath){
        
        managedObjectContext?.deleteObject(details[indexPath.row])
        details.removeAtIndex(indexPath.row)
        do {
            try managedObjectContext?.save()
            //edited here
            NSNotificationCenter.defaultCenter().postNotificationName(NSPersistentStoreCoordinatorStoresWillChangeNotification, object: nil)
        } catch let error as NSError {
            print(error)
            
        }
        flightDetailsTV.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
        
    }
    
    /****************************************
    Function to load the imageView with a default image when no image is selected from gallery or taken from camera
    *****************************************/
    
    func checkNillImage(img:UIImage)->UIImage
    {
        let image: UIImage? = img
        if image != nil {
            return image!
        }
        else
        {
            return UIImage(named: "AddImage.jpg")!
        }
    }
    
    /****************************************
    Search bar implementation
    *****************************************/
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchActive = false;
    }
    
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        let fetchRequest = NSFetchRequest(entityName:"FlightDetails")
        if searchText.isEmpty || searchText == ""{
            
            details = (try! managedObjectContext?.executeFetchRequest(fetchRequest)) as! [FlightDetails]
            details = Array(details.reverse())
            flightDetailsTV.reloadData()
            
        }
        else{
            if searchText.componentsSeparatedByString(":").count == 1{
                
                // all results predicate
                let resultPredicate = NSPredicate(format: "(from contains [c] %@) OR (ident contains [c] %@) OR (to contains [c] %@)", searchText,searchText,searchText)
                
                //date predicate
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "dd MMM yyyy"
                let date1 = dateFormatter.dateFromString(searchText)
                if date1 != nil {
                    
                    let datePredicate = NSPredicate(format: "date = %@",date1!)
                    
                    fetchRequest.predicate = NSCompoundPredicate(type : NSCompoundPredicateType.OrPredicateType, subpredicates :[datePredicate,resultPredicate])
                }
                else{
                    fetchRequest.predicate = resultPredicate
                }
            }
            else{
                let resultPredicate = NSPredicate(format: "(%K contains [c] %@)" ,searchText.componentsSeparatedByString(":")[0].lowercaseString,searchText.componentsSeparatedByString(":")[1].stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())+"")
                fetchRequest.predicate = resultPredicate
                
            }
            details = (try! managedObjectContext?.executeFetchRequest(fetchRequest)) as! [FlightDetails]
            details = Array(details.reverse())
            flightDetailsTV.reloadData()
            
        }
    }
    
}
