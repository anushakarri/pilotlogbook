//
//  FAAViewController.swift
//  PilotLogbook
//
//  Created by Admin on 9/30/15.
//  Copyright © 2015 NWMSU. All rights reserved.
//


/****************************************
This class is for displaying the documents available in a table
*****************************************/
import UIKit

class FAAViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var cellLabelsArray = ["Airman's Information Manual","Airport Facilities Directory","Weather Documents"]
    var pdfNameList = ["AFD","AIM","Faa-weather-docs"]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "FAA Documents"
        // Do any additional setup after loading the view.
    }

    @IBOutlet weak var pdfNamesTable: UITableView!
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
     func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
     func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 3
    }
    
    
     func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("pdfNameCell", forIndexPath: indexPath)
        cell.textLabel?.text = cellLabelsArray[indexPath.row]
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        Swift.print("in did select")
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        Swift.print()
        (segue.destinationViewController as! PDFReaderViewController).pdfName = pdfNameList[(pdfNamesTable.indexPathForSelectedRow!.row)]
       // Swift.print((segue.destinationViewController as! PDFReaderViewController).pdfName )
    }


}
