//
//  AircraftsViewController.swift
//  PilotLogboo1dk
//
//  Created by admin on 6/25/15.
//  Copyright (c) 2015 NWMSU. All rights reserved.
//


/******************************************
This class is used for getting the unique aircraft type and incrementing the day landings and night landings based
on aircraftType
*******************************************/

import UIKit
import CoreData

class AircraftViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UITextFieldDelegate {
    
    var managedObjectContext : NSManagedObjectContext! = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    var  details:[FlightDetails]!
    var fetchRequest : NSFetchRequest!
    var error : NSError?
    var aircraftTypes = Set<String>() // For displaying distinct aircraft types in the table
    var aircraftIdentArr  = [""]
    var annualBeforedateArr = ["date"]
    var dateFor100hrsArr = ["date"]
    var dateFormatter = NSDateFormatter()
    var selectedCellIndexPath : NSIndexPath!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchRequest = NSFetchRequest(entityName:"FlightDetails")
        dateFormatter.dateFormat = "dd MMM yyyy"
        //dismiss keyboard by tapping anywhere
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
        aircraftTypeSearchBar.delegate = self
        
    }
    
    override func viewWillAppear(animated: Bool) {
        fetchRequest = NSFetchRequest(entityName:"FlightDetails")
        
        details = (try! self.managedObjectContext?.executeFetchRequest(fetchRequest)) as! [FlightDetails]
        aircraftTypes = []
        dateFormatter.dateFormat = "dd MMM yyyy"
        for flight  in details{
            aircraftTypes.insert(flight.ident)
            //println(aircraftTypes)
        }
        aircraftIdentArr = []
        
        for aircraftType in aircraftTypes{
            aircraftIdentArr.append(aircraftType)
        }
        aircraftsTV.reloadData()
        annualBeforedateArr = []
        dateFor100hrsArr = []
        
    }
    
    
    @IBOutlet weak var aircraftsTV: UITableView!
    
    /****************************************
    Function to set the annual before date
    *****************************************/
    @IBAction func annualBeforeDate(sender: UITextField) {
        selectedCellIndexPath = getIndexPathOfElement(sender)
        let datePickerView  : UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.Date
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: Selector("handleDatePicker:"), forControlEvents: UIControlEvents.ValueChanged)
        sender.delegate = self
        
    }
    
    /****************************************
    Function to set the 100 hours date
    *****************************************/
    @IBAction func set100HrsDate(sender: UITextField) {
        selectedCellIndexPath = getIndexPathOfElement(sender)
        let datePickerView  : UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.Date
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: Selector("handleDatePicker2:"), forControlEvents: UIControlEvents.ValueChanged)
        sender.delegate = self
        //println((sender.superview?.subviews[3] as! UILabel).text)
    }
    
    
    /****************************************
    Function to set date in the textfield after formatting
    *****************************************/
    func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        let cell = aircraftsTV.cellForRowAtIndexPath(selectedCellIndexPath)! as UITableViewCell
        let tv = cell.viewWithTag(125) as! UITextField
        tv.text = dateFormatter.stringFromDate(sender.date)
        annualBeforedateArr[selectedCellIndexPath.row] = dateFormatter.stringFromDate(sender.date)
    }
    
    func handleDatePicker2(sender: UIDatePicker) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        let cell = aircraftsTV.cellForRowAtIndexPath(selectedCellIndexPath)! as UITableViewCell
        let tv = cell.viewWithTag(154) as! UITextField
        tv.text = dateFormatter.stringFromDate(sender.date)
        dateFor100hrsArr[selectedCellIndexPath.row] = dateFormatter.stringFromDate(sender.date)
       // Swift.print("dateFor100hrsArr is \(dateFor100hrsArr)")
    }
    
    //get the indexpath
    func getIndexPathOfElement(sender : UITextField)->NSIndexPath{
//        Swift.print((sender.superview?.viewWithTag(100) as! UILabel).text)
//        Swift.print("")
//        
        let label =  (sender.superview?.viewWithTag(101) as! UILabel).text
        var indexpath: NSIndexPath!
        for i in 0..<aircraftIdentArr.count{
//            Swift.print("items are \(label) and \(aircraftIdentArr[i])")
            if aircraftIdentArr[i] == label{
                indexpath = NSIndexPath(forRow: i, inSection: 0)
            }
        }
        
        return indexpath
    }
    
    // textFieldShouldReturn
    func textFieldShouldReturn(textField:UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBOutlet weak var aircraftTypeSearchBar: UISearchBar!
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        /******************************************
        Getting the unique aircraft type and incrementing the day landings and night landings based in aircraftType
        *******************************************/
        let aircraftTypePredicate = NSPredicate(format: "ident Like %@",aircraftIdentArr[indexPath.row])
        fetchRequest.predicate = aircraftTypePredicate
        details = (try! self.managedObjectContext?.executeFetchRequest(fetchRequest)) as! [FlightDetails]
        //Swift.print(details.count)
        var nightLanding = 0
        var dayLanding = 0
        var ident = ""
        var flightName = ""
        var annualBeforeDateStr = ""
        var datFor100HrsDate = ""
        for flight in details{
            nightLanding += flight.night_Landings.integerValue
            dayLanding += flight.day_Landings.integerValue
            ident = flight.ident
            flightName = flight.aircraft_Name
            annualBeforeDateStr = dateFormatter.stringFromDate(flight.annual_Before_Date)
            datFor100HrsDate = dateFormatter.stringFromDate(flight.i00hrsDate)
            
        }
        
        annualBeforedateArr.append(annualBeforeDateStr)
        dateFor100hrsArr.append(datFor100HrsDate)
        
        let cell = tableView.dequeueReusableCellWithIdentifier("aircraftCell", forIndexPath: indexPath)
        
        let dayLandingsLbl = cell.viewWithTag(102) as! UILabel
        let nightLandingsLbl = cell.viewWithTag(103) as! UILabel
        let identLbl = cell.viewWithTag(101) as! UILabel
        let aircraftTypeLbl = cell.viewWithTag(100) as! UILabel
        let annualBeforedate = cell.viewWithTag(125) as! UITextField
        let dateFor100hrs = cell.viewWithTag(154) as! UITextField
        
        dayLandingsLbl.text = dayLanding.description
        nightLandingsLbl.text = nightLanding.description
        identLbl.text = ident
        aircraftTypeLbl.text = flightName
        annualBeforedate.text = annualBeforedateArr[indexPath.row]
        dateFor100hrs.text = datFor100HrsDate
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aircraftTypes.count
    }
    
    /****************************************
    Search implementation for the aircrafts based on aircraft type
    *****************************************/
   
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        // To save the aircraft type details in the Set to an array.
        aircraftIdentArr = []
        fetchRequest = NSFetchRequest(entityName:"FlightDetails")
        if searchText != ""{
            let aircraftTypePredicate = NSPredicate(format: "(ident contains [c] %@) OR (aircraft_Name contains [c] %@)",searchText,searchText)
            fetchRequest.predicate = aircraftTypePredicate
        }
        
        details = (try! self.managedObjectContext?.executeFetchRequest(fetchRequest)) as! [FlightDetails]
        
        aircraftTypes = Set<String>()
        for flight  in details{
            aircraftTypes.insert(flight.ident)
            
        }
        self.aircraftIdentArr = []
        for i in aircraftTypes{
            self.aircraftIdentArr.append(i)
                    }
        aircraftsTV.reloadData()
        
    }
    
    /****************************************
    Function to save the annual before and 100 hours dates to the database after changing
    *****************************************/
    @IBAction func saveAircraftDetails(sender: AnyObject) {
        
        var predicate : NSPredicate!
        var flightEntryDataSet:[FlightDetails]!
        for i in 0..<aircraftIdentArr.count {
            predicate = NSPredicate(format: "ident contains [c] %@", aircraftIdentArr[i])
            fetchRequest.predicate = predicate
            
            flightEntryDataSet = (try! managedObjectContext!.executeFetchRequest(fetchRequest)) as! [FlightDetails]
            for flight in flightEntryDataSet{
                
                flight.annual_Before_Date = dateFormatter.dateFromString(annualBeforedateArr[i])!
                flight.i00hrsDate = dateFormatter.dateFromString(dateFor100hrsArr[i])!
                
            }
            do {
                try managedObjectContext?.save()
            } catch let error1 as NSError {
                error = error1
            }
        }
        
        
        
    }
    
    func dismissKeyboard(){
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
}
