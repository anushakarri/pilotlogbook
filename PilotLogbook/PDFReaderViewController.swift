//
//  PDFReaderViewController.swift
//  PilotLogbook
//
//  Created by Admin on 9/30/15.
//  Copyright © 2015 NWMSU. All rights reserved.
//

/****************************************
This class is for displaying selected pdf
*****************************************/
import UIKit

class PDFReaderViewController: UIViewController {

    @IBOutlet weak var pdfWebView: UIWebView!
 
    var pdfName : NSString!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let pdfLocation = NSURL(fileURLWithPath:NSBundle.mainBundle().pathForResource(pdfName.description, ofType:"pdf")!) //replace PDF_file with your pdf name
        let request = NSURLRequest(URL: pdfLocation);
        self.pdfWebView.loadRequest(request);
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
