//
//  AddFlightViewController.swift
//  PilotLogbook
//
//  Created by admin on 6/25/15.
//  Copyright (c) 2015 NWMSU. All rights reserved.
//

/****************************************
This class is used for adding the flight details
*****************************************/
import UIKit
import CoreData
import CoreGraphics

class AddFlightViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate , UIImagePickerControllerDelegate , UINavigationControllerDelegate {
    
    var managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    var selectedCellIndexPath : NSIndexPath = NSIndexPath(forRow: 0, inSection: 0)
    var selectedDate : NSDate!
    
    @IBOutlet weak var addDetailsTV: UITableView!
    @IBOutlet var datePickerTF: UITextField!
    var flightDetailArray  = Array2D(cols: 7, rows: 4) // initializing the array with 4 rows(since there are 4 sections in the table) and 7 columns(since a section has maximum of 7 rows)
    @IBOutlet var flightImg:UIImageView!
    var image:UIImage!
    enum tagValues : Int {
        case textlabelTag = 100, detailsTag = 101
    }
    var rowTextLableArray : [Array<String>]!
    
    
    /****************************************
    To load date in the textfield with the current date daily
    *****************************************/
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        let date1 : NSDate = NSDate()
        datePickerTF.text = dateFormatter.stringFromDate(date1)
        datePickerTF.delegate = self
        
        //dismiss keyboard by tapping anywhere
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
        
        //showing and hiding keyboard
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWasShown:", name: UIKeyboardDidShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillBeHidden:", name: UIKeyboardWillHideNotification, object: nil)
        
        //assigning the text labels for the array
        //section : Flight
        let section0RowHeaders = ["Aircraft Type","Ident","From","To","Route","Remarks","Instrument Approaches"]
        
        //section : Aircraft category
        let section1RowHeaders = ["Airplane SEL","Airplane MEL", "Cross Country"]
        
        //section : Conditions of flight
        let section2RowHeaders = ["Day Landings","Night Landings","Actual Instruments","Simulated Instruments"]
        
        //section : Types of piloting
         let section3RowHeaders = ["Dual Received", "Pilot in Command", "Total Time"]
        
         rowTextLableArray  = [section0RowHeaders,section1RowHeaders,section2RowHeaders,section3RowHeaders]
        
        selectedDate = NSDate()
     }
    
    /****************************************
    Action for taking image with camera roll
    *****************************************/
    @IBAction func selectCamera(sender: AnyObject) {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = .Camera
        presentViewController(picker, animated: true, completion: nil)
    }
    
    /****************************************
    Action for selecting image from gallery
    *****************************************/
    @IBAction func galleryClicked(sender: AnyObject) {
        let picker = UIImagePickerController()
        picker.sourceType = .PhotoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypesForSourceType(.PhotoLibrary)!
        picker.delegate = self
        picker.allowsEditing = true
        self.presentViewController(picker, animated: true, completion: nil)
        
    }
    
    /****************************************
    Function for picking an image or camera
    *****************************************/
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        let image:UIImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        let screenSize:CGSize = UIScreen.mainScreen().bounds.size
        let newImage:UIImage = scalePhoto(image, size: CGSize(width: screenSize.width, height: screenSize.height))
        self.image = newImage
        flightImg.image = self.image
        picker.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    /****************************************
    Function for cancelling the image picker
    *****************************************/
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        
        picker.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    /****************************************
    Function for scaling the image
    *****************************************/
    func scalePhoto(image:UIImage, size:CGSize) ->UIImage
    {
        let scale:CGFloat = max(size.width/image.size.width,size.height/image.size.height)
        let width:CGFloat = image.size.width * scale
        let height:CGFloat = image.size.height * scale
        let imageRect:CGRect = CGRectMake((size.width - width)/2.0, (size.height - height)/2.0, width, height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        image.drawInRect(imageRect)
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
    
    /****************************************
    Action for date to appear in the textfield
    *****************************************/
    @IBAction func datePickerFunc(sender: UITextField) {
        let datePickerView  : UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.Date
        datePickerView.date = NSDate()
        //datePickerView.locale = NSLocale.systemLocale()
        //datePickerView.locale = NSLocale.d
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: Selector("handleDatePicker:"), forControlEvents: UIControlEvents.ValueChanged)
    }
    
    /****************************************
    Function to set date in the textfield after formatting
    *****************************************/
    
    func handleDatePicker(sender: UIDatePicker) {
        Swift.print("")
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy HH mm ss"
    
        datePickerTF.text = (dateFormatter.stringFromDate(sender.date) as NSString).substringToIndex(11)
        
        selectedDate = dateFormatter.dateFromString(dateFormatter.stringFromDate(sender.date))
    }
    
    override func viewDidAppear(animated:Bool) {
        super.viewDidAppear(animated)
        autoFill() // for auto filling aircraft type and ident
    }
    
    override func viewWillDisappear(animated: Bool) {
        image = UIImage(named: "AddImage.jpg")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /****************************************
    Function for adding flight to the database
    *****************************************/
    @IBAction func done(sender:AnyObject) {
        
        var emptyFieldCheck = true
        if flightDetailArray[0,0].description.isEmpty {
            let alert = UIAlertView()
            alert.title = "Alert"
            alert.message = "Please enter the aircraft type"
            alert.addButtonWithTitle("Ok")
            alert.show()
            emptyFieldCheck = false
        }
        if flightDetailArray[0,1].description.isEmpty && emptyFieldCheck == true{
            let alert = UIAlertView()
            alert.title = "Alert"
            alert.message = "Please enter the aircraft ident"
            alert.addButtonWithTitle("Ok")
            alert.show()
            emptyFieldCheck = false
        }
        if flightDetailArray[0,2].description.isEmpty && emptyFieldCheck == true{
            let alert = UIAlertView()
            alert.title = "Alert"
            alert.message = "Please enter the origin"
            alert.addButtonWithTitle("Ok")
            alert.show()
            emptyFieldCheck = false
        }
        if flightDetailArray[0,3].description.isEmpty && emptyFieldCheck == true{
            let alert = UIAlertView()
            alert.title = "Alert"
            alert.message = "Please enter the destination"
            alert.addButtonWithTitle("Ok")
            alert.show()
            emptyFieldCheck = false
        }
        
        // adding entries to the database
        if emptyFieldCheck == true {
            let flightEntry:FlightDetails = NSEntityDescription.insertNewObjectForEntityForName("FlightDetails", inManagedObjectContext: managedObjectContext!) as! FlightDetails
            
            flightEntry.aircraft_Name = self.flightDetailArray[0,0].description
            flightEntry.ident = self.flightDetailArray[0,1].description
            flightEntry.from = self.flightDetailArray[0,2].description
            flightEntry.to = self.flightDetailArray[0,3].description
            flightEntry.route = self.flightDetailArray[0,4].description
            flightEntry.remarks = self.flightDetailArray[0,5].description
            flightEntry.no_Of_Instruments = Int(self.flightDetailArray[0,6].integerValue)
            flightEntry.airplaneSEL = decimalConvertor(self.flightDetailArray[1,0].doubleValue)
            flightEntry.airplaneMEL = decimalConvertor(self.flightDetailArray[1,1].doubleValue)
            flightEntry.cross_Country = self.flightDetailArray[1,2].doubleValue
            flightEntry.day_Landings = (self.flightDetailArray[2,0].integerValue)
            flightEntry.night_Landings = self.flightDetailArray[2,1].integerValue
            flightEntry.actual_Instruments = decimalConvertor(self.flightDetailArray[2,1].doubleValue)
            flightEntry.simulated_Instruments = decimalConvertor(self.flightDetailArray[2,2].doubleValue)
            flightEntry.dual_Received = decimalConvertor(self.flightDetailArray[3,0].doubleValue)
            flightEntry.pilot_In_Command = decimalConvertor(self.flightDetailArray[3,1].doubleValue)
            flightEntry.total_Time = decimalConvertor(self.flightDetailArray[3,2].doubleValue)
            let modifiedDate = NSDate()
            flightEntry.modifiedDate = modifiedDate
            flightEntry.annual_Before_Date = modifiedDate
            flightEntry.i00hrsDate = modifiedDate
            
            // checking for image
            if(self.image != nil)
            {
                flightEntry.image = UIImagePNGRepresentation(checkNillImage(self.image!))!
            }
            else
            {
                flightEntry.image = UIImagePNGRepresentation(UIImage(named: "AddImage.jpg")!)!
            }
            
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "dd MMM yyyy"
            let date = selectedDate
            flightEntry.date = date!
            
            do {
                try managedObjectContext!.save()
                NSNotificationCenter.defaultCenter().postNotificationName(NSPersistentStoreCoordinatorStoresWillChangeNotification, object: nil)
            } catch  {
               
            }
            self.dismissViewControllerAnimated(true , completion: nil)
            self.navigationController?.popViewControllerAnimated(true)
        }
        
    }
    
    /****************************************
    Action to dismiss view controller
    *****************************************/
    @IBAction func cancel(sender: AnyObject) {
        self.dismissViewControllerAnimated(true , completion: nil)
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    // Again I wonder why this needs to be so large and so messy. As usual, use control I to fix the indentation.
    
    /****************************************
    Function for loading the labels with the names by their respective sections with rows
    *****************************************/
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("detailCell", forIndexPath: indexPath)
        let textLabel : UILabel = cell.viewWithTag(tagValues.textlabelTag.rawValue) as! UILabel
        let detailsTF : UITextField = cell.viewWithTag(tagValues.detailsTag.rawValue) as! UITextField
        //label1.autocapitalizationType = UITextAutocapitalizationType.Words
        detailsTF.delegate = self
        textLabel.text = ((rowTextLableArray[indexPath.section])[indexPath.row]).stringByAppendingString(":")
        detailsTF.placeholder = (rowTextLableArray[indexPath.section])[indexPath.row]
        
        if !(indexPath.section == 0 && indexPath.row != 6){
            detailsTF.keyboardType = UIKeyboardType.NumberPad
        }
        return cell
    }
    
    
    /****************************************
    Funciton to return the number of rows based on the section
    *****************************************/
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rowsInSectionArray = [7,3,4,3]
        return rowsInSectionArray[section]
    }
    
    
    /****************************************
    Function to return number of sections
    *****************************************/
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 4
    }
    
    /****************************************
    Function for giving titles for headers in the sections
    *****************************************/
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var tileForSectionsArray = ["Flight","Aircraft Category","Conditions of Flight","Type of Piloting"]
        return tileForSectionsArray[section]
        
    }
    
    /****************************************
    Function for giving the height of the header in a section
    *****************************************/
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45.0
    }
    
    /****************************************
    Function for giving height of row in a section
    *****************************************/
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 43.0
    }
    
    // textFieldShouldReturn
    func textFieldShouldReturn(textField:UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    /****************************************
    Function to limit decimal values to one
    *****************************************/
    
    func decimalConvertor( var value :Double)-> Double{
        value *= 10
        let nstr : NSString = value.description
        value = Double(nstr.integerValue) * 0.1
        return value    }
    
    /****************************************
    Function which handles when a tap is recognised on the screen
    *****************************************/
    
    func dismissKeyboard(){
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        
        view.endEditing(true)
    }
    
    /****************************************
    Function for capitalizing the first five rows in the table
    *****************************************/
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        let indexpath =  self.addDetailsTV.indexPathForCell(textField.superview?.superview as! UITableViewCell)
        self.selectedCellIndexPath = indexpath! // This will be used for showing and hiding keyboard when keyboard blocks a textfield in the table
        
        //validating the input fields
        let  char = string.cStringUsingEncoding(NSUTF8StringEncoding)!
        switch indexpath!.section
            
        {
            //section 0
        case 0:
            switch indexpath!.row{
            case 6:
                if !((char[0] >= 48 && char[0] <= 57) || char[0] == 0){
                    automaticALert(2,alertMessage : "Enter only integer values for instrument approaches")
                    return false
                    
                }
                
            default: print("", terminator: "")
            }
            
            
            //section 1
        case 1:
            switch indexpath!.row{
                
            case 0,1,2 :if !((char[0] >= 48 && char[0] <= 57) || char[0] == 0 ){
                if (char[0] == 46){
                    //check for old decimals
                    let str : NSString = textField.description
                    let noOFDecimals  = str.componentsSeparatedByString(".")
                    //   println(noOFDecimals.count)
                    if(noOFDecimals.count > 2){
                        
                        automaticALert(2,alertMessage : "Enter proper decimal values")
                        return false
                        
                    }
                }
                else{
                    automaticALert(2,alertMessage : "Enter only decimal values")
                    return false
                    
                }
                }
            default: print("", terminator: "")
            }
            
            
            
            //section 2
        case 2: switch indexpath!.row{
        case 0 :
            if !((char[0] >= 48 && char[0] <= 57) || char[0] == 0){
                automaticALert(2,alertMessage : "Enter only integer values for Day Landings")
                return false
                
            }
        case 1 :
            if !((char[0] >= 48 && char[0] <= 57) || char[0] == 0){
                automaticALert(2,alertMessage : "Enter only integer values for Night Landings")
                return false
                
            }
            
        case 2,3 :if !((char[0] >= 48 && char[0] <= 57) || char[0] == 0 ){
            if (char[0] == 46){
                //check for old decimals
                let str : NSString = textField.description
                let noOFDecimals  = str.componentsSeparatedByString(".")
                if(noOFDecimals.count > 2){
                    
                    automaticALert(2,alertMessage : "Enter proper decimal values")
                    return false
                    
                }
            }
            else{
                automaticALert(2,alertMessage : "Enter only decimal values")
                return false
                
            }
            }
            
            
            
        default: print("", terminator: "")
            }
            
            
            //section 3
            
        case 3: switch indexpath!.row{
        case 0,1,2 :if !((char[0] >= 48 && char[0] <= 57) || char[0] == 0 ){
            if (char[0] == 46){
                //check for old decimals
                let str : NSString = textField.description
                let noOFDecimals  = str.componentsSeparatedByString(".")
                if(noOFDecimals.count > 2){
                    
                    
                    automaticALert(2,alertMessage : "Enter proper decimal values")
                    return false
                    
                }
            }
            else{
                automaticALert(2,alertMessage : "Enter only decimal values")
                return false
                
            }
            }
        default : print("", terminator: "")
            }
            
        default: print("", terminator: "")
        }
        
        // Capitalization of first five fields
        if indexpath!.section == 0 && indexpath!.row < 5{
            let substring : NSString = (textField.text as NSString!).stringByReplacingCharactersInRange(range, withString: string )
            textField.text = substring.uppercaseString
            self.flightDetailArray[indexpath!.section,indexpath!.row] = textField.text!
        }
        else{
            let substring : NSString = (textField.text as NSString!).stringByReplacingCharactersInRange(range, withString: string )
            if substring.length > 0 {
                let firstLetter = substring.substringWithRange(NSMakeRange(0, 1))
                textField.text = firstLetter.capitalizedString + substring.substringFromIndex(1)
                self.flightDetailArray[indexpath!.section,indexpath!.row] = textField.text!
            }
            else{
                textField.text = ""
            }
        }
        
        return false
    }
    
    /****************************************
    Function to load the imageView with a default image when no image is selected from gallery or taken from camera
    *****************************************/
    
    func checkNillImage(img:UIImage)->UIImage
    {
        let image: UIImage? = img //UIImage(contentsOfFile: "")
        if image != nil {
            return image!
        }
        else
        {
            return UIImage(named: "AddImage.jpg")!
        }
    }
    
    /****************************************
    Function for autofilling aircraft type and ident
    The data which is populated based on the last entered record
    *****************************************/
    func autoFill(){
        let fetchRequest:NSFetchRequest! = NSFetchRequest(entityName: "FlightDetails")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key : "modifiedDate", ascending : false)]
        var lastEntry = (try! managedObjectContext!.executeFetchRequest(fetchRequest)) as! [FlightDetails]
        
        if lastEntry.count > 0  {
            
            var index = NSIndexPath(forRow : 0, inSection : 0)
            var cell = addDetailsTV.cellForRowAtIndexPath(index)!
            (cell.viewWithTag(101) as! UITextField).text = lastEntry[0].aircraft_Name
            index = NSIndexPath(forRow : 1, inSection : 0)
            cell = addDetailsTV.cellForRowAtIndexPath(index)!
            (cell.viewWithTag(101) as! UITextField).text = lastEntry[0].ident
            
            self.flightDetailArray[0,0] = lastEntry[0].aircraft_Name
            self.flightDetailArray[0,1] = lastEntry[0].ident
            
        }
    }
    
    
    /****************************************
    Function to show the keyboard and scroll the table up when the keyboard is trying to hide the fields
    *****************************************/
    func keyboardWasShown (notification: NSNotification)
    {
        var info = notification.userInfo!
        let keyboardSize = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue().size
        var contentInsets:UIEdgeInsets
        
        if UIInterfaceOrientationIsPortrait(UIApplication.sharedApplication().statusBarOrientation) {
            contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0);
        }
        else {
            contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.width, 0.0);
            
        }
        
        addDetailsTV.contentInset = contentInsets
        addDetailsTV.scrollToRowAtIndexPath(selectedCellIndexPath, atScrollPosition: UITableViewScrollPosition.Top, animated: true)
        addDetailsTV.scrollIndicatorInsets = addDetailsTV.contentInset
    }
    
    /****************************************
    Function to hide the keyboard
    *****************************************/
    func keyboardWillBeHidden (notification: NSNotification)
    {
        addDetailsTV.contentInset = UIEdgeInsetsZero
        addDetailsTV.scrollIndicatorInsets = UIEdgeInsetsZero
    }
    
    /****************************************
    Alert function to be cancelled after some time automatically
    ****************************************/
    func automaticALert(time : Int, alertMessage : NSString){
        let alert = UIAlertView()
        alert.title = "Alert"
        alert.message = alertMessage.description
        alert.addButtonWithTitle("Ok")
        alert.show()
        
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(2 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            alert.dismissWithClickedButtonIndex(-1, animated: true)
        }
    }
    
}
