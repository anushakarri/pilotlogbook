//
//  HistoryViewController.swift
//  PilotLogbook
//
//  Created by admin on 6/25/15.
//  Copyright (c) 2015 NWMSU. All rights reserved.
//

/**
textLablesArray should be textLabelsArray - correct the spelling.
There should be a comment at the top of this class explaining what it is (it might be obvious, but it still needs to be here).
I am not convinced you need switch 0/1/2 for total, day and night landings - there should be a more elegant way to do this.
If you do need to use 0/1/2, set up an enum.
Here too, fix indentation with control I.
*/

import UIKit
import CoreData

/*****************************************
This class shows the summary of the number of hours flown and the number of day and night landings
******************************************/
class HistoryViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    let textLabelsArray = ["Total hours flown","Last 7 days","This month","Last 60 days","Last 90 days","Day landings in last 90 days","Night landings in last 90 days"]
    // no of days for each type of selection
    var daysForEashSelectionArr = [-1,7,30,60,90,90,90]
    let searchType = [0,0,0,0,0,1,2]
    
    @IBOutlet weak var historyTV: UITableView!
    var managedObjectContext : NSManagedObjectContext! = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    let calendar = NSCalendar.currentCalendar()
    let components = NSDateComponents()
    var fetchRequest : NSFetchRequest!
    var error : NSError?
    var currentDate = NSDate()
    var  details:[FlightDetails]!
    var totalHours = 0.0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let date = NSDate()
        let components = calendar.components([.Month, .Day], fromDate: date)
        daysForEashSelectionArr = [-1,7,components.day,60,90,90,90]
        fetchRequest = NSFetchRequest(entityName:"FlightDetails")
        self.tabBarItem.title = "Summary"
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        historyTV.reloadData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("historyCell", forIndexPath: indexPath)
        let textLabel : UILabel = cell.viewWithTag(100) as! UILabel
        
        let hoursLabel : UILabel = cell.viewWithTag(101) as! UILabel
        
        
        textLabel.text = textLabelsArray[indexPath.row]
        if indexPath.row < 5{
            hoursLabel.text = generateData(daysForEashSelectionArr[indexPath.row], type: searchType[indexPath.row]).description
        }else{
            hoursLabel.text = Int(generateData(daysForEashSelectionArr[indexPath.row], type: searchType[indexPath.row])).description
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    // days is number of days
    // type is for differentiating total hours of nightLandings or dayLandings
    func generateData(days : Int, type : Int)-> Double{
        
        totalHours = 0
        if days > -1{
            components.weekday = -days
            let selectedDay = calendar.dateByAddingComponents(components, toDate: currentDate, options: [])
            let datePredicate = NSPredicate(format: "date > %@",selectedDay!)
            fetchRequest.predicate = datePredicate
            details = (try! self.managedObjectContext?.executeFetchRequest(fetchRequest)) as! [FlightDetails]
            for flight in details{
                switch type {
                case 0 : totalHours += flight.total_Time.doubleValue
                case 1: totalHours += flight.day_Landings.doubleValue
                case 2 : totalHours += flight.night_Landings.doubleValue
                default : print("")
                }
            }
        }
        else{
            details = (try! self.managedObjectContext?.executeFetchRequest(fetchRequest)) as! [FlightDetails]
            for flightdetails in details{
                totalHours += flightdetails.total_Time.doubleValue
            }
        }
        return totalHours
    }
}
