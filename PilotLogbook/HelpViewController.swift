//
//  HelpViewController.swift
//  PilotLogbook
//
//  Created by admin on 6/25/15.
//  Copyright (c) 2015 NWMSU. All rights reserved.
//

/****************************************
This class is used for creating the main screen of the help tab
*****************************************/

import UIKit

class HelpViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("helpCell", forIndexPath: indexPath) 
        let textLabel : UILabel = cell.viewWithTag(100) as! UILabel
        switch indexPath.row {
        case 0 :  textLabel.text = "Logbook"
        case 1 : textLabel.text = "History"
        case 2 : textLabel.text = "My Aircraft"
        case 3 : textLabel.text = "Profile"
        case 4 : textLabel.text = "FAA Documents"
        default: ""
        }
        return cell
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == 0{
        let logbookHelp:LogbookHelpViewController = self.storyboard?.instantiateViewControllerWithIdentifier("logbookHelp") as! LogbookHelpViewController
        self.navigationController?.pushViewController(logbookHelp, animated: true)
        }
        else if indexPath.row == 1{
        let historyHelp:HistoryHelpViewController = self.storyboard?.instantiateViewControllerWithIdentifier("historyHelp") as! HistoryHelpViewController
        self.navigationController?.pushViewController(historyHelp, animated: true)
        }
         else if indexPath.row == 2{
        let aircraftHelp:AircraftsHelpViewController = self.storyboard?.instantiateViewControllerWithIdentifier("aircraftHelp") as! AircraftsHelpViewController
        self.navigationController?.pushViewController(aircraftHelp, animated: true)
        }else if indexPath.row == 3{
        let profileHelp:ProfileHelpViewController = self.storyboard?.instantiateViewControllerWithIdentifier("profileHelp") as! ProfileHelpViewController
        self.navigationController?.pushViewController(profileHelp, animated: true)
        }
        else{
            let faaDocuments : FAAViewController  = self.storyboard?.instantiateViewControllerWithIdentifier("faaDocument") as! FAAViewController
            self.navigationController?.pushViewController(faaDocuments, animated: true)
        }
        
    }
}
