//
//  LogbookDetailViewController.swift
//  PilotLogbook
//
//  Created by admin on 6/25/15.
//  Copyright (c) 2015 NWMSU. All rights reserved.
//

/****************************************
This class is for showing the details each flight of the record
*****************************************/
import UIKit
import CoreData
import Social

class LogbookDetailViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    
    var appy = UIApplication.sharedApplication().delegate as! AppDelegate
    var managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    var image1:UIImage!
    var counter = 0
    var flightDetails: FlightDetails!
    var flightDetailsArray : [NSString] = Array(count: 18, repeatedValue: "")
    var selectedCellIndexPath : NSIndexPath = NSIndexPath(forRow: 0, inSection: 0)
    var rowTextLabelArray : [NSString]!
    var selectedDate : NSDate!
    
    enum tagValues : Int {
        case textlabelTag = 100, detailsTag = 101
    }
    
    @IBOutlet weak var editDetailsTV: UITableView!
    @IBOutlet weak var originalImage: UIImageView!
    
    /****************************************
    Loading the flight details into the arrayfor displaying in the detail view when the view is loaded
    *****************************************/
    override func viewDidLoad() {
        super.viewDidLoad()
        flightDetails = appy.logbookDetail
        
        // loading the flight details into the array for displaying in the detail view
        flightDetailsArray[0] = flightDetails.aircraft_Name
        flightDetailsArray[1] = flightDetails.ident
        flightDetailsArray[2] = flightDetails.from
        flightDetailsArray[3] = flightDetails.to
        flightDetailsArray[4] = flightDetails.route
        flightDetailsArray[5] = flightDetails.remarks
        flightDetailsArray[6] = flightDetails.no_Of_Instruments.description
        
        flightDetailsArray[7] = flightDetails.airplaneSEL.description
        flightDetailsArray[8] = flightDetails.airplaneMEL.description
        flightDetailsArray[9] = flightDetails.cross_Country.description
        
        flightDetailsArray[10] = flightDetails.day_Landings.description
        flightDetailsArray[11] = flightDetails.night_Landings.description
        flightDetailsArray[12] = flightDetails.actual_Instruments.description
        flightDetailsArray[13] = flightDetails.simulated_Instruments.description
        
        
        flightDetailsArray[14] = flightDetails.dual_Received.description
        flightDetailsArray[15] = flightDetails.pilot_In_Command.description
        flightDetailsArray[16] = flightDetails.total_Time.description
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        flightDetailsArray[17] = dateFormatter.stringFromDate( flightDetails.date)
        
        let imageTap : UITapGestureRecognizer = UITapGestureRecognizer(target : self , action : "tappingOnImage")
        originalImage.addGestureRecognizer(imageTap)
        originalImage.userInteractionEnabled = true
        
        self.navigationItem.title="Flight Details"
        
        //dismiss keyboard by tapping anywhere
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: UIBarButtonItemStyle.Plain, target: self, action: "save:")
        
        //showing and hiding keyboard
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWasShown:", name: UIKeyboardDidShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillBeHidden:", name: UIKeyboardWillHideNotification, object: nil)
        
        //assigning the text labels for the array
       
        
        rowTextLabelArray  = ["Aircraft Type","Ident","From","To","Route","Remarks","Instrument Approaches","Airplane SEL","Airplane MEL", "Cross Country","Day Landings","Night Landings","Actual Instruments","Simulated Instruments","Dual Received", "Pilot in Command", "Total Time","Date"]

        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /****************************************
    To load the image when the view is about to appear
    *****************************************/
    override func viewWillAppear(animated: Bool) {
        if(appy.isImageEdit == true)
        {
            if(appy.editImage != nil)
            {
                originalImage.image = appy.editImage
            }
            else
            {
                originalImage.image = UIImage(named: "AddImage.jpg")!
            }
        }
        else
        {
            if(image1 != nil)
            {
                originalImage.image = image1
            }
            else
            {
                originalImage.image = UIImage(named: "AddImage.jpg")!
            }
            
        }
        
        appy.isImageEdit = false
    }
    
    /****************************************
    Function for loading the table with the flight record values from the database
    *****************************************/
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("detailCell", forIndexPath: indexPath)
        let textLabel : UILabel = cell.viewWithTag(tagValues.textlabelTag.rawValue) as! UILabel
        let detailsTF : UITextField = cell.viewWithTag(tagValues.detailsTag.rawValue) as! UITextField
        detailsTF.delegate = self
        
        textLabel.text = (rowTextLabelArray[indexPath.row]).stringByAppendingString(":")
        detailsTF.text = flightDetailsArray[indexPath.row].description
        if indexPath.row == 17{
            Swift.print("")
            let datePickerView  : UIDatePicker = UIDatePicker()
            datePickerView.datePickerMode = UIDatePickerMode.Date
            datePickerView.date = NSDate()
            //datePickerView.locale = NSLocale.systemLocale()
            //datePickerView.locale = NSLocale.d
            detailsTF.inputView = datePickerView
            
            datePickerView.addTarget(self, action: Selector("handleDatePicker:"), forControlEvents: UIControlEvents.ValueChanged)
            
        }
            
        return cell
        
    }
    
    func handleDatePicker(sender: UIDatePicker) {
        Swift.print("")
        let formatter = NSDateFormatter()
        formatter.dateFormat = "dd MMM yyyy HH mm ss"
        Swift.print(sender.superview)
    
//        datePickerTF.text = (dateFormatter.stringFromDate(sender.date) as NSString).substringToIndex(11)
//        
        selectedDate = formatter.dateFromString(formatter.stringFromDate(sender.date))
        flightDetails.date = selectedDate
       let cell =  self.editDetailsTV.cellForRowAtIndexPath(NSIndexPath(forRow: 17, inSection: 0))!
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"

        (cell.viewWithTag(tagValues.detailsTag.rawValue) as! UITextField).text = dateFormatter.stringFromDate(selectedDate)
        //(cell.viewWithTag(tagValues.detailsTag.rawValue) as! UITextField).resignFirstResponder()
        
    }

    
    /****************************************
    Function to return the number of sections in the table view
    *****************************************/
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    /****************************************
    Function to return the number of rows in a particular section
    *****************************************/
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 18
        
        
    }
    
    /****************************************
    Function to return the height of a row
    *****************************************/
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 45.0
    }
    
    // textFieldShouldReturn
    func textFieldShouldReturn(textField:UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    
    /****************************************
    Function for capitalization of first five rows in the table view
    *****************************************/
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        let indexpath =  self.editDetailsTV.indexPathForCell(textField.superview?.superview as! UITableViewCell)
        self.selectedCellIndexPath = indexpath!
        
        
        let  char = string.cStringUsingEncoding(NSUTF8StringEncoding)!
        
        switch indexpath!.row
            
        {
            //row 6 check for integers only condition
                    case 6:
                if !((char[0] >= 48 && char[0] <= 57) || char[0] == 0){
                    automaticALert(2,alertMessage : "Enter only integer values for Instrument Approaches")
                    return false
                    
                }
                
         //row 10 check for integers only condition with custom message
          case 10 :
            if !((char[0] >= 48 && char[0] <= 57) || char[0] == 0){
                automaticALert(2,alertMessage : "Enter only integer values for Day Landings")
                return false
                
            }
            //row 11 check for integers only condition with custom message
        case 11 :
            if !((char[0] >= 48 && char[0] <= 57) || char[0] == 0){
                automaticALert(2,alertMessage : "Enter only integer values for Night Landings")
                return false
                
            }
          //check for decimals only
        case 7,8,9,12,13,14,15,16 :if !((char[0] >= 48 && char[0] <= 57) || char[0] == 0 ){
            if (char[0] == 46){
                //check for old decimals
                let contentString : NSString = textField.description
                let noOFDecimals  = contentString.componentsSeparatedByString(".")
                if(noOFDecimals.count > 2){
                    
                    automaticALert(2,alertMessage : "Enter proper decimal values")
                    return false
                    
                }
            }
            else{
                automaticALert(2,alertMessage : "Enter only decimal values")
                return false
                
            }
            }
            
        default: print("")
        }
        
    
        let substring : NSString = (textField.text as NSString!).stringByReplacingCharactersInRange(range, withString: string )
        // Capitalization of first five fields
        if  indexpath!.row < 5{
            
            textField.text = substring.uppercaseString
            self.flightDetailsArray[indexpath!.row] = textField.text!
        }
        else if indexpath?.row  == 5{
            
            if substring.length > 0 {
                let firstLetter = substring.substringWithRange(NSMakeRange(0, 1))
                textField.text = firstLetter.capitalizedString + substring.substringFromIndex(1)
                self.flightDetailsArray[indexpath!.row] = textField.text!
            }
            else{
                textField.text = ""
                self.flightDetailsArray[indexpath!.row] = textField.text!
            }
            
        }
        else{
            
            self.flightDetailsArray[indexpath!.row] = substring
            return true
        }
        
        return false
        
    }
    
    
    /****************************************
    Function to save the edited flight record values to the database
    *****************************************/
    func save(sender : UIBarButtonItem){
        
        // editing entries to the database
        let fetchRequest = NSFetchRequest(entityName: "FlightDetails")
        let flightEntryDataSet = (try! managedObjectContext!.executeFetchRequest(fetchRequest)) as! [FlightDetails]
        
        for flightEntry in flightEntryDataSet{
            if flightEntry.objectID == flightDetails.objectID {
                flightEntry.aircraft_Name = flightDetailsArray[0].description
                flightEntry.ident = flightDetailsArray[1].description
                flightEntry.from = flightDetailsArray[2].description
                flightEntry.to = flightDetailsArray[3].description
                flightEntry.route = flightDetailsArray[4].description
                flightEntry.remarks = flightDetailsArray[5].description
                flightEntry.no_Of_Instruments = Int(flightDetailsArray[6].intValue)
                
                flightEntry.airplaneSEL = decimalConvertor(flightDetailsArray[7].doubleValue)
                
                
                flightEntry.airplaneMEL = decimalConvertor(flightDetailsArray[8].doubleValue)
                flightEntry.cross_Country = decimalConvertor(flightDetailsArray[9].doubleValue)
                flightEntry.day_Landings = Int(flightDetailsArray[10].intValue)
                flightEntry.night_Landings = Int(flightDetailsArray[11].intValue)
                flightEntry.actual_Instruments = decimalConvertor(flightDetailsArray[12].doubleValue)
                flightEntry.simulated_Instruments = decimalConvertor(flightDetailsArray[13].doubleValue)
                flightEntry.dual_Received = decimalConvertor(flightDetailsArray[14].doubleValue)
                flightEntry.pilot_In_Command = decimalConvertor(flightDetailsArray[15].doubleValue)
                flightEntry.total_Time = decimalConvertor(flightDetailsArray[16].doubleValue)
                // var dateFormatter = NSDateFormatter()
                //dateFormatter.dateFormat = "dd MMM yyyy"
                
                // flightEntry.date = dateFormatter.dateFromString(flightDetailsArray[17] as String)!
                if(appy.editImage != nil)
                {
                    flightEntry.image = UIImagePNGRepresentation(appy.editImage)!
                }
            }
        }
        do {
            try managedObjectContext?.save()
        } catch let error as NSError {
           print(error)
        }
        //managedObjectContext?.save(&error)
        self.dismissViewControllerAnimated(true , completion: nil)
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    /****************************************
    Function to limit the decimal values to one
    *****************************************/
    func decimalConvertor( var value :Double)-> Double{
        value *= 10
        let nstr : NSString = value.description
        value = Double(nstr.integerValue) * 0.1
        return value
    }
    
    /****************************************
    Function called when a tap gesture is recognised
    *****************************************/
    func dismissKeyboard(){
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    /****************************************
    Function to handle when image is tapped to get a full image view in the view controller
    *****************************************/
    func tappingOnImage()
    {
        let fullImageVC = self.storyboard?.instantiateViewControllerWithIdentifier("fullimage") as! FullPhotoViewController
        fullImageVC.image2 = image1
        
        self.navigationController?.pushViewController(fullImageVC, animated: true )
    }
    
    
//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//        if(segue.identifier == "editImgSegue")
//        {
//            let fullPhotoDVC = segue.destinationViewController as! FullPhotoViewController
//        }
//    }
    
    /****************************************
    Function to load the imageView with a default image when no image is selected from gallery or taken from camera
    *****************************************/
    func checkNillImage(img:UIImage)->UIImage
    {
        let image: UIImage? = img
        if image != nil {
            return image!
        }
        else
        {
            return UIImage(named: "AddImage.jpg")!
        }
    }
    
    /****************************************
    Function to show the keyboard and scroll the table up when the keyboard is trying to hide the fields
    *****************************************/
    func keyboardWasShown (notification: NSNotification)
    {
        var info = notification.userInfo!
        let keyboardSize = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue().size
        var contentInsets:UIEdgeInsets
        
        if UIInterfaceOrientationIsPortrait(UIApplication.sharedApplication().statusBarOrientation) {
            contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0);
        }
        else {
            contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.width, 0.0);
            
        }
        
        editDetailsTV.contentInset = contentInsets
        editDetailsTV.scrollToRowAtIndexPath(selectedCellIndexPath, atScrollPosition: UITableViewScrollPosition.Top, animated: true)
        editDetailsTV.scrollIndicatorInsets = editDetailsTV.contentInset
    }
    
    /****************************************
    Function to hide the keyboard
    *****************************************/
    func keyboardWillBeHidden (notification: NSNotification)
    {
        editDetailsTV.contentInset = UIEdgeInsetsZero
        editDetailsTV.scrollIndicatorInsets = UIEdgeInsetsZero
    }
    
    
    /****************************************
    Alert function to be cancelled after some time automatically
    *****************************************/
    func automaticALert(time : Int, alertMessage : NSString){
        let alert = UIAlertView()
        alert.title = "Alert"
        alert.message = alertMessage.description
        alert.addButtonWithTitle("Ok")
        alert.show()
        
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(2 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            alert.dismissWithClickedButtonIndex(-1, animated: true)
        }
        
    }
    
    /****************************************
    Function to share the flight details on to Facebook
    *****************************************/
    @IBAction func shareToFaceBook(sender: AnyObject) {
        let shareToFB: SLComposeViewController = SLComposeViewController(forServiceType : SLServiceTypeFacebook)
        shareToFB.setInitialText("I just flew from \(flightDetailsArray[2]) to \(flightDetailsArray[3]) in \(flightDetailsArray[16]) hours. Wish you could've been there!")
        self.presentViewController(shareToFB, animated: true, completion: nil)
        
    }
    
    /****************************************
    Function to share flight details to Twitter
    *****************************************/
    @IBAction func shareToTwitter(sender: AnyObject) {
        let shareToTwitter: SLComposeViewController = SLComposeViewController(forServiceType : SLServiceTypeTwitter)
        shareToTwitter.setInitialText("I just flew from \(flightDetailsArray[2]) to \(flightDetailsArray[3]) in \(flightDetailsArray[16]) hours. Wish you could've been there!")
        self.presentViewController(shareToTwitter, animated: true, completion: nil)
        
    }
    
}
