 //
 //  ProfileViewController.swift
 //  PilotLogbook
 //
 //  Created by admin on 6/25/15.
 //  Copyright (c) 2015 NWMSU. All rights reserved.
 //
 
 import UIKit
 import EventKit
 
 
 /****************************************
 This class shows the profile of the user to set dates for next biennial and next medical exam
 *****************************************/
 class ProfileViewController: UIViewController{
    
    @IBOutlet weak var biennialFlightReviewDate: UIDatePicker!
    
    @IBOutlet weak var medicalExamDate: UIDatePicker!
    
    @IBOutlet weak var biennialFlightReviewSwitch: UISwitch!
    @IBOutlet weak var medicalExamSwitch: UISwitch!
    var background : UIBackgroundTaskIdentifier?
    
    var notificationStartDate: NSDate!
    var secondNotificationDate: NSDate!
    var thirdNotificationDate: NSDate!
    
    var biennialFlightReviewNotificationDates : [NSDate]!
    var medicalExamNotificationDates : [NSDate]!
    
    @IBOutlet weak var medicalExamNotificationButton: UIButton!
    
    @IBOutlet weak var biennialFlightReviewButton: UIButton!
    @IBAction func biennialFlightReviewNotification(sender: AnyObject)
    {
        
        
        if biennialFlightReviewSwitch.on
        {
            biennialFlightReviewButton.enabled = true
            biennialFlightReviewNotificationDates = []
            let reviewDate = self.biennialFlightReviewDate.date
            if reviewDate.compare(NSDate()).rawValue == -1{
                callAlert("Select Date after current date")
                biennialFlightReviewButton.enabled = false
                biennialFlightReviewSwitch.setOn(false, animated: true)
            }
            
            let formatter = NSDateFormatter()
            formatter.dateFormat = "MM/dd/YYYY"
            let day = formatter.stringFromDate(reviewDate)
            let today = formatter.dateFromString(formatter.stringFromDate(NSDate()))
            notificationStartDate = NSDate(timeInterval: -90*24*60*60 , sinceDate: reviewDate)
            
            if notificationStartDate.compare(NSDate()).rawValue >= 0{
                biennialFlightReviewNotificationDates.append(notificationStartDate)
                Swift.print("i came ra idiot")
                let firstNotification = UILocalNotification()
                firstNotification.alertBody = "Your biennial flight review is pending on \(day)"
                firstNotification.alertTitle = "Next biennial flight review date"
                firstNotification.fireDate = notificationStartDate
                UIApplication.sharedApplication().scheduleLocalNotification(firstNotification)
            }
            
            secondNotificationDate = NSDate(timeInterval: -60*24*60*60, sinceDate: reviewDate)
            if secondNotificationDate.compare(NSDate()).rawValue >= 0{
                biennialFlightReviewNotificationDates.append(secondNotificationDate)
                let secondNotification = UILocalNotification()
                secondNotification.alertBody = "Your biennial flight review is pending on \(day)"
                secondNotification.alertTitle = "Next biennial flight review date"
                secondNotification.fireDate = secondNotificationDate
                UIApplication.sharedApplication().scheduleLocalNotification(secondNotification)
            }
            
            thirdNotificationDate = NSDate(timeInterval: -30*24*60*60 , sinceDate: reviewDate)
            if  thirdNotificationDate.compare(NSDate()).rawValue >= 0{
                biennialFlightReviewNotificationDates.append(thirdNotificationDate)
                let thirdNotification = UILocalNotification()
                thirdNotification.alertBody = "Your biennial flight review is pending on \(day)"
                thirdNotification.alertTitle = "Next biennial flight review date"
                thirdNotification.fireDate = thirdNotificationDate
                UIApplication.sharedApplication().scheduleLocalNotification(thirdNotification)
            }
            
            
            let finalNotification = UILocalNotification()
            finalNotification.alertBody = "Your biennial flight review is today"
            finalNotification.alertTitle = "Next biennial flight review date"
            finalNotification.fireDate = reviewDate
            biennialFlightReviewNotificationDates.append(reviewDate)
            if reviewDate.compare(today!).rawValue >= 0{
            UIApplication.sharedApplication().scheduleLocalNotification(finalNotification)
            }
        }
        else{
            biennialFlightReviewButton.enabled = false
        }
    }
    
    @IBAction func medicalExamNotification(sender: AnyObject) {
        if medicalExamSwitch.on
        {
             medicalExamNotificationButton.enabled = true
             medicalExamNotificationDates = []
            let reviewDate = self.medicalExamDate.date
                    if reviewDate.compare(NSDate()).rawValue == -1{
                callAlert("Select date after current date")
                        medicalExamNotificationButton.enabled = false
                        medicalExamSwitch.setOn(false, animated: true)
            }
            let formatter = NSDateFormatter()
            formatter.dateFormat = "MM/dd/YYYY"
            
            let day = formatter.stringFromDate(reviewDate)
            let today = formatter.dateFromString(formatter.stringFromDate(NSDate()))
           
            
            let notificationStartDate = NSDate(timeInterval: -90*24*60*60, sinceDate: reviewDate)
            if notificationStartDate.compare(NSDate()).rawValue >= 0{
                medicalExamNotificationDates.append(notificationStartDate)
                let firstNotification = UILocalNotification()
                firstNotification.alertBody = "Your next medical exam is on \(day)"
                firstNotification.alertTitle = "Next medical exam"
                firstNotification.fireDate = notificationStartDate
                UIApplication.sharedApplication().scheduleLocalNotification(firstNotification)
            }
            
            let secondNotificationDate = NSDate(timeInterval: -60*24*60*60, sinceDate: reviewDate)
            if secondNotificationDate.compare(NSDate()).rawValue >= 0{
                medicalExamNotificationDates.append(secondNotificationDate)
                let secondNotification = UILocalNotification()
                secondNotification.alertBody = "Your next medical exam is on \(day)"
                secondNotification.alertTitle = "Next medical exam"
                secondNotification.fireDate = secondNotificationDate
                UIApplication.sharedApplication().scheduleLocalNotification(secondNotification)
            }
            
            
            let thirdNotificationDate = NSDate(timeInterval: -30*24*60*60, sinceDate: reviewDate)
            if thirdNotificationDate.compare(NSDate()).rawValue >= 0{
                medicalExamNotificationDates.append(thirdNotificationDate)
                let thirdNotification = UILocalNotification()
                thirdNotification.alertBody = "Your next medical exam is on \(day)"
                thirdNotification.alertTitle = "Next medical exam"
                thirdNotification.fireDate = thirdNotificationDate
               
                UIApplication.sharedApplication().scheduleLocalNotification(thirdNotification)
            }
            
            let finalNotification = UILocalNotification()
            finalNotification.alertBody = "You have a medical exam today"
            finalNotification.alertTitle = "Medical exam"
            finalNotification.fireDate = reviewDate
            if reviewDate.compare(today!).rawValue >= 0{
                medicalExamNotificationDates.append(reviewDate)
                UIApplication.sharedApplication().scheduleLocalNotification(finalNotification)
            }
            
        }
        else{
             medicalExamNotificationButton.enabled = false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.sharedApplication().beginBackgroundTaskWithExpirationHandler({UIApplication.sharedApplication().endBackgroundTask(self.background!)})
        UIApplication.sharedApplication().beginBackgroundTaskWithExpirationHandler({UIApplication.sharedApplication().endBackgroundTask(self.background!)})
        biennialFlightReviewNotificationDates = []
        medicalExamNotificationDates = []
        
        biennialFlightReviewButton.enabled = false
        medicalExamNotificationButton.enabled = false
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    @IBAction func saveToICalendar(sender: UIButton) {
        
        let eventStore = EKEventStore()
        
        switch EKEventStore.authorizationStatusForEntityType(EKEntityType.Event) {
        case .Authorized:
            if(sender == medicalExamNotificationButton){
                insertEventForMedical(eventStore)
            }
            else{
            insertEvent(eventStore)
            }
        case .Denied:
            Swift.print("Access denied")
        case .NotDetermined:
            // 3
            eventStore.requestAccessToEntityType(EKEntityType.Event, completion:
                {[weak self](granted: Bool, error: NSError?) -> Void in
                    if granted {
                        if (sender == self!.medicalExamNotificationDates){
                           self!.insertEventForMedical(eventStore)
                        }else{
                        self!.insertEvent(eventStore)
                        }
                    } else {
                        Swift.print("Access denied")
                    }
                })
        default:
            Swift.print("Case Default")
        }
    }
    
    func insertEvent(store: EKEventStore){
        let calendars = store.calendarsForEntityType(EKEntityType.Event)
        let formatter = NSDateFormatter()
        formatter.dateFormat = "MM/dd/YYYY"
        
        
        for calender in calendars{
            Swift.print(calender.title)
            if calender.title == "Home"{
                var  event = EKEvent(eventStore: store)
                
                for date in self.biennialFlightReviewNotificationDates{
                    event = EKEvent(eventStore: store)
                    event.calendar = calender
                    event.title = "You have next biennial flight review on \(formatter.stringFromDate(self.biennialFlightReviewDate.date))"
                    event.startDate = date
                    event.endDate = date.dateByAddingTimeInterval(2*60*60)
                    Swift.print(date)
                    
                    do {
                        try store.saveEvent(event, span: EKSpan.ThisEvent)
                    }catch{
                        
                    }
                }
                
            }
        }
    }
    
    func insertEventForMedical(store: EKEventStore){
        let calendars = store.calendarsForEntityType(EKEntityType.Event)
        let formatter = NSDateFormatter()
        formatter.dateFormat = "MM/dd/YYYY"
        
        for calender in calendars{
            Swift.print(calender.title)
            if calender.title == "Home"{
                var  event = EKEvent(eventStore: store)
                
                for date in self.medicalExamNotificationDates{
                    event = EKEvent(eventStore: store)
                    event.calendar = calender
                    event.title = "You have next medical exam on \(formatter.stringFromDate(self.medicalExamDate.date))"
                    event.startDate = date
                    event.endDate = date.dateByAddingTimeInterval(2*60*60)
                    Swift.print(date)
                    
                    do {
                        try store.saveEvent(event, span: EKSpan.ThisEvent)
                        
                    }catch{
                        
                    }
                }
                
            }
        }
    }
    
    

    
    
    /****************************************
    Alert function to be cancelled after some time automatically
    ****************************************/
    func callAlert(alertMessage : NSString){
        let alert = UIAlertView()
        alert.title = "Alert"
        alert.message = alertMessage.description
        alert.addButtonWithTitle("Ok")
        alert.show()
        
    }
    
 }
   