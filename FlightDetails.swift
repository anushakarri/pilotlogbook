//
//  FlightDetails.swift
//  
//
//  Created by Admin on 9/14/15.
//
//

import Foundation
import CoreData

class FlightDetails: NSManagedObject {

    @NSManaged var actual_Instruments: NSNumber
    @NSManaged var aircraft_Name: String
    @NSManaged var airplaneMEL: NSNumber
    @NSManaged var airplaneSEL: NSNumber
    @NSManaged var cross_Country: NSNumber
    @NSManaged var date: NSDate
    @NSManaged var day_Landings: NSNumber
    @NSManaged var dual_Received: NSNumber
    @NSManaged var from: String
    @NSManaged var ident: String
    @NSManaged var image: NSData
    @NSManaged var modifiedDate: NSDate
    @NSManaged var night_Landings: NSNumber
    @NSManaged var no_Of_Instruments: NSNumber
    @NSManaged var pilot_In_Command: NSNumber
    @NSManaged var remarks: String
    @NSManaged var route: String
    @NSManaged var simulated_Instruments: NSNumber
    @NSManaged var to: String
    @NSManaged var total_Time: NSNumber
    @NSManaged var annual_Before_Date: NSDate
    @NSManaged var i00hrsDate: NSDate

}
